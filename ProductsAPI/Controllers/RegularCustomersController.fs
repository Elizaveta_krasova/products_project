namespace FSharpApiSample.Controllers

open System.Data
open Dapper
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging

type PgRegularCustomer = {
    id: int
    first_name: string
    last_name: string
    patronymic: string
}

type CreateRegularCustomerRequest = {
    first_name: string
    last_name: string
    patronymic: string
}

type DeleteRegularCustomerRequest = {
    id: int
}

[<ApiController>]
[<Route("api/regular_customers")>]
type RegularCustomersController(logger: ILogger<RegularCustomersController> ,pgConnection : IDbConnection) =
    inherit ControllerBase()
    
    [<HttpGet>]
    member x.GetRegularCustomers() = task {
        try
            let! regularCustomers = pgConnection.QueryAsync<PgRegularCustomer>("Select * from regular_customers;")
            return regularCustomers |> ActionResult<seq<PgRegularCustomer>>
        with
        | err ->
            logger.LogError("GetRegularCustomers: {err}", err)
            return x.BadRequest() |> ActionResult<seq<PgRegularCustomer>>
    }
    
    [<HttpPost("create")>]
    member x.CreateRegularCustomers([<FromBody>] body : CreateRegularCustomerRequest) = task {
        try
            let query = """
INSERT INTO regular_customers (first_name, last_name, patronymic) 
VALUES (@first_name, @last_name, @patronymic) returning id, first_name, last_name, patronymic;
"""
            let parametrs = dict [("first_name", box body.first_name)
                                  ("last_name", body.last_name); ("patronymic", body.patronymic)]
            let! result = pgConnection.QueryAsync<PgRegularCustomer>(query, parametrs)
            
            return Seq.head result |> ActionResult<PgRegularCustomer>
        with
        | err ->
            logger.LogError("CreateRegularCustomers: {err}", err)
            return x.BadRequest() |> ActionResult<PgRegularCustomer>
    }
    
    [<HttpPost("update")>]
    member x.UpdateRegularCustomers([<FromBody>] body : PgRegularCustomer) = task {
        try
            let query = """
Update regular_customers 
set first_name = @first_name, last_name = @last_name, patronymic = @patronymic 
where id = @id 
returning id, first_name, last_name, patronymic;
"""
            let parametrs =  dict [("first_name", box body.first_name); ("last_name", body.last_name);
                                   ("patronymic", body.patronymic); ("id", body.id)]
            let! result = pgConnection.QueryAsync<PgRegularCustomer>(query, parametrs)
            
            return Seq.head result |> ActionResult<PgRegularCustomer>
        with
        | err ->
            logger.LogError("UpdateRegularCustomers: {err}", err)
            return x.BadRequest() |> ActionResult<PgRegularCustomer>
    }
    
    [<HttpDelete("delete")>]
    member x.DeleteRegularCustomers([<FromBody>] body : DeleteRegularCustomerRequest) = task {
        try
            let query = """
DELETE FROM regular_customers 
       WHERE id = @id
       returning id,  first_name, last_name, patronymic;
"""
            let parametrs =  dict [("id", box body.id)]
            let! result = pgConnection.QueryAsync<PgRegularCustomer>(query, parametrs)
            
            return Seq.head result |> ActionResult<PgRegularCustomer>
        with
        | err ->
            logger.LogError("UpdateRegularCustomers: {err}", err)
            return x.BadRequest() |> ActionResult<PgRegularCustomer>
    }