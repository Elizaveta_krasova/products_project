namespace FSharpApiSample

open System
open System.Data
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open Npgsql

module Program =
    let private pgHost = Environment.GetEnvironmentVariable "POSTGRES_HOST"
    let private pgPort = Environment.GetEnvironmentVariable "POSTGRES_PORT"
    let private pgDB = Environment.GetEnvironmentVariable "POSTGRES_USER"
    let private pgUser = Environment.GetEnvironmentVariable "POSTGRES_DB"
    let private pgPassword = Environment.GetEnvironmentVariable "POSTGRES_PASSWORD"

    let private pgConnectionString =
        $"Server={pgHost};Port={pgPort};Database={pgDB};User Id={pgUser};Password={pgPassword};"

    [<EntryPoint>]
    let main args =
        let builder = WebApplication.CreateBuilder args

        builder.Services.AddControllers() |> ignore
        builder.Services.AddEndpointsApiExplorer() |> ignore
        builder.Services.AddSwaggerGen() |> ignore

        builder.Services.AddSpaStaticFiles(fun conf -> conf.RootPath <- "wwwroot")

        let frontendDevPolicy = "frontend_dev_policy"
        builder.Services.AddCors(fun options ->
            options.AddPolicy(frontendDevPolicy, fun policyBuilder ->
                policyBuilder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader() |> ignore)) |> ignore

        builder.Services.AddScoped<IDbConnection>(fun _ -> new NpgsqlConnection(pgConnectionString)) |>ignore

        let app = builder.Build()

        if app.Environment.IsDevelopment() then
            app.UseCors(frontendDevPolicy) |> ignore

        app.UseStaticFiles() |> ignore
        app.UseRouting() |> ignore
        app.UseEndpoints(fun builder -> builder.MapControllers() |> ignore) |> ignore

        app.UseSwagger() |> ignore
        app.UseSwaggerUI() |> ignore

        app.Map("", fun (applicationBuilder: IApplicationBuilder) ->
            applicationBuilder.UseSpa(fun spaBuilder -> spaBuilder.Options.SourcePath <- "/wwwroot")) |> ignore

        app.Run()

        0