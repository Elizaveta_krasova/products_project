import {createContext} from "react"
import {makeAutoObservable, runInAction} from "mobx";

interface RegularCustomers {
    id: number
    first_name: string
    last_name: string
    patronymic: string
}

const API_URL = process.env.REACT_APP_API_URL ?? ''

class RegularCustomersState {
    loadState: 'pending' | 'success' | 'error' = 'pending'
    saveState = false

    regularCustomers: RegularCustomers[] = []

    constructor() {
        makeAutoObservable(this);
    }

    changeFirstName(id: number, name: string) {
        this.regularCustomers.find(cust => cust.id === id)!.first_name = name;
    }

    changeLastName(id: number, lastName: string) {
        this.regularCustomers.find(cust => cust.id === id)!.last_name = lastName;
    }

    changePatronymic(id: number, patronymic: string) {
        this.regularCustomers.find(cust => cust.id === id)!.patronymic = patronymic;
    }

    async fetchRegularCustomers() {
        this.loadState = 'pending'

        try {
            const res = await fetch(`${API_URL}/api/regular_customers`)

            if (res.ok) {
                const result = await res.json()

                runInAction(() => {
                    this.regularCustomers = result;
                    this.loadState = 'success'
                })
            } else {
                runInAction(() => {
                    this.loadState = 'error'
                })
            }
        } catch (err) {
            runInAction(() => {
                this.loadState = 'error'
            })
        }


    }

    async createRegularCustomer() {
        const res = await fetch(`${API_URL}/api/regular_customers/create`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({first_name: "", last_name: "", patronymic: ""})
        })

        if (res.ok) {
            const {id} = await res.json()
            this.regularCustomers.push({id, first_name: "", last_name: "", patronymic: ""})
            return true
        } else {
            return false
        }
    }

    async deleteRegularCustomer(id: number) {
        const res = await fetch(
            `${API_URL}/api/regular_customers/delete?id=${id}`,
            {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({id})
            }
        )

        if (res.ok) {
            runInAction(() => {
                this.regularCustomers = this.regularCustomers.filter(r => r.id !== id)
            })
            return true
        } else return false
    }

    async updateRegularCustomer(id: number) {
        this.saveState = true

        try {
            const res = await fetch(`${API_URL}/api/regular_customers/update`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(this.regularCustomers.find(cust => cust.id === id)!)
            })

            runInAction(() => {
                this.saveState = false
            })

            return res.ok
        } catch (err) {
            runInAction(() => {
                this.saveState = false
            })

            return false
        }

    }

}

export const regularCustomersState = createContext(new RegularCustomersState())

