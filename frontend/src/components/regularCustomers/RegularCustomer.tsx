import {observer} from "mobx-react-lite";
import {useContext} from "react";
import {regularCustomersState} from "./RedularCustomersState";
import styles from "/home/elizaveta/frontend/src/components/regularCustomers/RegularCustomer.module.css";

interface RegularCustomerProps {
    id: number
}

export const RegularCustomer = observer(({ id }: RegularCustomerProps) => {
    const context = useContext(regularCustomersState)
    const regularCustomer = context.regularCustomers.find(r => r.id === id)!

    return (
        <div key={regularCustomer.id}>
            <span>{regularCustomer.id}</span>
            <input placeholder="Default input"
                   disabled={context.saveState}
                   value={regularCustomer.first_name}
                   className={[styles.regularCustomers, 'form-control'].join(' ')}
                   onChange={event => {
                       context.changeFirstName(regularCustomer.id, event.target.value)
                   }}
            />

            <input
                value={regularCustomer.last_name}
                disabled={context.saveState}
                className={[styles.regularCustomers, 'form-control'].join(' ')}
                onChange={event => {
                    context.changeLastName(regularCustomer.id, event.target.value)
                }}
            />

            <input
                value={regularCustomer.patronymic}
                disabled={context.saveState}
                className={[styles.regularCustomers, 'form-control'].join(' ')}
                onChange={event => {
                    context.changePatronymic(regularCustomer.id, event.target.value)
                }}
            />

            <button
                className={styles.button10}
                disabled={context.saveState}
                onClick={async () => {
                    const updated = await context.updateRegularCustomer(regularCustomer.id)
                    updated ? alert("Сохранение прошло успешно") : alert("Не удалось сохранить")
                }}
            >
                Сохранить

            </button>

            <button
                className={styles.button10}
                disabled={context.saveState}
                onClick={async () => {
                    if (window.confirm("Удалить?")) {
                        const deleted = await context.deleteRegularCustomer(regularCustomer.id)
                        deleted ? alert("Удалено!") : alert("Не удалось")
                    }
                }}
            >
                Удалить
            </button>
        </div>
    )
})