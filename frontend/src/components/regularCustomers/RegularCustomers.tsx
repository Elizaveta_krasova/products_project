import {observer} from "mobx-react-lite";
import {useContext} from "react";
import {regularCustomersState} from "./RedularCustomersState";
import styles from "/home/elizaveta/frontend/src/components/regularCustomers/RegularCustomer.module.css";
import {RegularCustomer} from "./RegularCustomer";

export const RegularCustomers = observer(() => {
    const context = useContext(regularCustomersState)

    return (
        <>
            {context.loadState === 'pending' && (
                <>
                    <div className={styles.loaderWrapper}>
                        <div className={styles.loader}></div>
                        <p>Идёт загрузка ...</p>
                    </div>

                </>
            )}
            {context.loadState === 'success' && (
                <>
                    {context.regularCustomers.map(regularCustomer => (
                        <RegularCustomer key={regularCustomer.id} id={regularCustomer.id}/>
                    ))}

                    <div className={styles.centerButton}>
                        <button
                            disabled={context.saveState}
                            className={styles.button10}
                            onClick={async () => {
                                const created = await context.createRegularCustomer()
                                created ? alert("Покупатель создан") : alert("Произошла ошибка")
                            }}
                        >
                            Добавить покупателя
                        </button>
                    </div>
                </>
            )}
            {context.loadState === 'error' && (
                <>
                    <span>Произошла ошибка при загрузке...</span>
                    <button onClick={async () => {
                        await context.fetchRegularCustomers()
                    }}
                    >
                        Повторить загрузку
                    </button>
                </>
            )}
            {context.saveState && <p>Идёт загрузка ...</p>}
        </>
    )
})