import {useContext, useEffect } from "react"
import {
    Route, useNavigate, BrowserRouter, Routes,
} from "react-router-dom";
import {observer} from "mobx-react-lite";
import {regularCustomersState} from "./components/regularCustomers/RedularCustomersState";
import {RegularCustomers} from "./components/regularCustomers/RegularCustomers";

const Index = () => {
    let navigate = useNavigate()

    return (
        <>
            <p>Стартовая страница</p>
            <button onClick={() => navigate('regular_customers')}>Перейти к покупателям</button>
        </>
    )
}

const App = observer(() => {
    const context = useContext(regularCustomersState)

    useEffect(() => {
        context.fetchRegularCustomers()
    }, [])

    return <BrowserRouter>
        <Routes>
            <Route path='/' element={<Index/>}/>
            <Route path='/regular_customers' element={<RegularCustomers/>}/>
        </Routes>
    </BrowserRouter>
})


export default App
